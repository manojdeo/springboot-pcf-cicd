package com.manoj.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPcfCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPcfCicdApplication.class, args);
	}

}
